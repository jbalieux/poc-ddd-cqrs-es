<?php

namespace App\Controller;

use App\Accounts\Users\Application\CreateUserCommand;
use App\Accounts\Users\Application\DeleteUserCommand;
use App\Accounts\Users\Application\FindUserQuery;
use App\Accounts\Users\Application\FindUsersQuery;
use App\Accounts\Users\Application\UpdateUserCommand;
use App\Shared\Application\Command;
use App\Shared\Application\CommandBus;
use App\Shared\Application\Query;
use App\Shared\Application\QueryBus;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Exception\NotEncodableValueException;
use Symfony\Component\Serializer\SerializerInterface;

class TestController extends AbstractController
{
    private Request $request;

    public function __construct(
        private RequestStack $requestStack,
        private readonly CommandBus $commandBus,
        private readonly QueryBus $queryBus,
        private readonly SerializerInterface $serializer
    ) {
        $this->request = $this->requestStack->getMainRequest();
    }

    #[Route('/test/users', name: "test_users_post", methods: ['POST'])]
    public function createUser(): JsonResponse
    {
        return $this->dddJson(CreateUserCommand::class, 201);
    }

    #[Route('/test/users', name: "test_users_get", methods: ['GET'])]
    public function getUsers(): JsonResponse
    {
        return $this->dddJson(FindUsersQuery::class);
    }


    #[Route('/test/users/{id}', name: "test_users_get_one", methods: ['GET'])]
    public function findUser(string $id): JsonResponse
    {
        return $this->dddJson(FindUserQuery::class, entityId: $id);
    }

    #[Route('/test/users/{id}', name: "test_users_patch", methods: ['PATCH'])]
    public function updateUser(string $id): JsonResponse
    {
        return $this->dddJson(UpdateUserCommand::class, entityId: $id);
    }

    #[Route('/test/users/{id}', name: "test_users_delete", methods: ['DELETE'])]
    public function deleteUser(string $id): JsonResponse
    {
        return $this->dddJson(DeleteUserCommand::class, 204, $id);
    }

//    #[Route('/test/test', name: "test_test", methods: ['GET'])]
//    #[DddCommand(FindUserCommand::class, statusCode: 202)]
//    public function test(): JsonResponse
//    {
//        $class = new \ReflectionMethod($this, __FUNCTION__);
//        $routeAttributes = $class->getAttributes(DddCommand::class);
//        $dddRoute = $routeAttributes[0]->newInstance();
//        if (!$dddRoute instanceof DddCommand) {
//            throw new \RuntimeException();
//        }
//        return $this->dddJson($dddRoute->getClassName(), $dddRoute->getStatusCode());
//    }

    private function dddJson(string $className, int $statusCode = 200, string $entityId = null): JsonResponse
    {
        $requestContent = $this->request->getContent();
        if (empty($requestContent)) {
            if ($entityId) {
                $command = new $className($entityId);
            } else {
                $command = new $className();
            }
        } else {
            try {
                if ($entityId) {
                    $object = json_decode($this->request->getContent());
                    $object->id = $entityId;
                    $command = $this->serializer->deserialize(
                        json_encode($object),
                        $className,
                        'json'
                    );
                } else {
                    $command = $this->serializer->deserialize(
                        $this->request->getContent(),
                        $className,
                        'json'
                    );
                }
            } catch (NotEncodableValueException $exception) {
                throw new BadRequestHttpException('This route must not receive a payload!');
            }

        }
        if ($command instanceof Command) {
            $response = $this->commandBus->dispatch($command);
        } elseif ($command instanceof Query) {
            $response = $this->queryBus->dispatch($command);
        } else {
            throw new \LogicException("There's no bus for this class");
        }

        return $this->json($response->value, $statusCode);
    }
}
