<?php

namespace App\Shared\Application;


use App\Shared\Domain\DomainEvent;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Serializer\SerializerInterface;

class EventBus
{
    const FILE_PATH = '/var/log/accounts.log';

    /**
     * @var EventHandler[]
     */
    private array $eventHandlers;

    public function __construct(
        \Traversable $eventHandlers,
        private readonly KernelInterface $kernel,
        private readonly SerializerInterface $serializer
    ) {
        $this->eventHandlers = iterator_to_array($eventHandlers);
    }

    public function dispatch(DomainEvent $event): void
    {
        $fp = fopen($this->kernel->getProjectDir() . self::FILE_PATH, 'a');
        fputs($fp, $this->serializer->serialize(new EventLogLine($event), 'json') . PHP_EOL);
        fclose($fp);

        $eventClassName = get_class($event);
        foreach ($this->eventHandlers as $eventHandler) {
            /** @var CommandHandler $eventHandler */
            if ($eventHandler->listenTo() !== $eventClassName) {
                continue;
            }
            $eventHandler->handle($event);
        }
    }
}
