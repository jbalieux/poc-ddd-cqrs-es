<?php

namespace App\Shared\Application;

class QueryResponse
{
    private function __construct(public readonly mixed $value)
    {
    }

    public static function withValue($value): self
    {
        return new self($value);
    }
}
