<?php

namespace App\Shared\Application;


class CommandBus
{
    /**
     * @var CommandHandler[]
     */
    private array $commandHandlers;

    public function __construct(\Traversable $commandHandlers)
    {
        $this->commandHandlers = iterator_to_array($commandHandlers);
    }

    public function dispatch(Command $command): CommandResponse
    {
        $commandClassName = get_class($command);
        foreach ($this->commandHandlers as $commandHandler) {
            /** @var CommandHandler $commandHandler */
            if ($commandHandler->listenTo() !== $commandClassName) {
                continue;
            }
            return $commandHandler->handle($command);
        }
        throw new \LogicException(sprintf("There's no handler for this command! actual: %s", $commandClassName));
    }
}
