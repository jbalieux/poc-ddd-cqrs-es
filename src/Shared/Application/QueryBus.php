<?php

namespace App\Shared\Application;


class QueryBus
{
    /**
     * @var QueryHandler[]
     */
    private array $queryHandlers;

    public function __construct(\Traversable $queryHandlers)
    {
        $this->queryHandlers = iterator_to_array($queryHandlers);
    }

    public function dispatch(Query $query): QueryResponse
    {
        $queryClassName = get_class($query);
        foreach ($this->queryHandlers as $queryHandler) {
            /** @var CommandHandler $queryHandler */
            if ($queryHandler->listenTo() !== $queryClassName) {
                continue;
            }
            return $queryHandler->handle($query);
        }
        throw new \LogicException(sprintf("There's no handler for this query! actual: %s", $queryClassName));
    }
}
