<?php

namespace App\Shared\Application;

use App\Shared\Domain\DomainEvent;

class EventLogLine
{
    public string $eventName;
    public \DateTimeImmutable $appendAt;
    public string $eventVersion;

    public function __construct(public readonly DomainEvent $event)
    {
        $this->eventName = get_class($this->event);
        $this->appendAt = new \DateTimeImmutable();
        $this->eventVersion = '0.0.1';
    }
}
