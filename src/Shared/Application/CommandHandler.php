<?php

namespace App\Shared\Application;

interface CommandHandler
{
    public function handle(Command $command): CommandResponse;

    public function listenTo(): string;
}
