<?php

namespace App\Shared\Application;

use App\Shared\Domain\DomainEvent;

interface EventHandler
{
    public function handle(DomainEvent $event): void;

    public function listenTo(): string;
}
