<?php

namespace App\Shared\Application;

interface QueryHandler
{
    public function handle(Query $query): QueryResponse;

    public function listenTo(): string;
}
