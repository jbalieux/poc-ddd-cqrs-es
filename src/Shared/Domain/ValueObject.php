<?php

namespace App\Shared\Domain;

interface ValueObject
{
    public function getValue();
}
