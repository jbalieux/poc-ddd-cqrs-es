<?php

namespace App\Accounts\Users\Application;

use App\Shared\Application\Command;

class CreateUserCommand implements Command
{
    public function __construct(public readonly string $email)
    {
    }
}
