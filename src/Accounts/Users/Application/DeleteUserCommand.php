<?php

namespace App\Accounts\Users\Application;

use App\Accounts\Users\Domain\UserId;
use App\Shared\Application\Command;

class DeleteUserCommand implements Command
{
    public UserId $id;

    public function __construct(string $id)
    {
        $this->id = UserId::fromUuidString($id);
    }
}
