<?php

namespace App\Accounts\Users\Application;

use App\Accounts\Users\Domain\UserRepository;
use App\Shared\Application\Query;
use App\Shared\Application\QueryHandler;
use App\Shared\Application\QueryResponse;

class FindUserQueryHandler implements QueryHandler
{
    public function __construct(private readonly UserRepository $repository)
    {
    }

    public function handle(Query $query): QueryResponse
    {
        if (!$query instanceof FindUserQuery) {
            throw new \InvalidArgumentException();
        }
        return QueryResponse::withValue($this->repository->find($query->id));
    }

    public function listenTo(): string
    {
        return FindUserQuery::class;
    }

}
