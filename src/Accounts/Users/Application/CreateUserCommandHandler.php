<?php

namespace App\Accounts\Users\Application;

use App\Accounts\Users\Domain\UserCreated;
use App\Shared\Application\Command;
use App\Accounts\Users\Domain\User;
use App\Accounts\Users\Domain\UserId;
use App\Accounts\Users\Domain\UserRepository;
use App\Shared\Application\CommandHandler;
use App\Shared\Application\CommandResponse;
use App\Shared\Application\EventBus;

class CreateUserCommandHandler implements CommandHandler
{
    public function __construct(
        private readonly UserRepository $repository,
        private readonly EventBus $eventBus
    ) {
    }

    public function handle(Command $command): CommandResponse
    {
        if (!$command instanceof CreateUserCommand) {
            throw new \InvalidArgumentException();
        }

        $user = new User(UserId::new(), $command->email);

        $this->repository->save($user);

        $this->eventBus->dispatch(new UserCreated($user));

        return CommandResponse::withValue($user->getId());
    }

    public function listenTo(): string
    {
        return CreateUserCommand::class;
    }

}
