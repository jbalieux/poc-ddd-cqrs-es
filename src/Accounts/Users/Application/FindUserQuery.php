<?php

namespace App\Accounts\Users\Application;

use App\Accounts\Users\Domain\UserId;
use App\Shared\Application\Query;

class FindUserQuery implements Query
{
    public UserId $id;

    public function __construct(string $id)
    {
        $this->id = UserId::fromUuidString($id);
    }
}
