<?php

namespace App\Accounts\Users\Application;

use App\Accounts\Users\Domain\UserRepository;
use App\Shared\Application\Command;
use App\Shared\Application\CommandHandler;
use App\Shared\Application\CommandResponse;

class DeleteUserCommandHandler implements CommandHandler
{
    public function __construct(private readonly UserRepository $repository)
    {
    }

    public function handle(Command $command): CommandResponse
    {
        if (!$command instanceof DeleteUserCommand) {
            throw new \InvalidArgumentException();
        }
        $this->repository->delete($command->id);
        return CommandResponse::withValue(true);
    }

    public function listenTo(): string
    {
        return DeleteUserCommand::class;
    }

}
