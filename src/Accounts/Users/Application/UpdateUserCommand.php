<?php

namespace App\Accounts\Users\Application;

use App\Accounts\Users\Domain\UserId;
use App\Shared\Application\Command;

class UpdateUserCommand implements Command
{
    public UserId $id;

    public function __construct(string $id, public readonly string $email)
    {
        $this->id = UserId::fromUuidString($id);
    }
}
