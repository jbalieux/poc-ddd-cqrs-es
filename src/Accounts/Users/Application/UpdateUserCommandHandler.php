<?php

namespace App\Accounts\Users\Application;

use App\Accounts\Users\Domain\UserUpdated;
use App\Shared\Application\Command;
use App\Accounts\Users\Domain\UserRepository;
use App\Shared\Application\CommandHandler;
use App\Shared\Application\CommandResponse;
use App\Shared\Application\EventBus;

class UpdateUserCommandHandler implements CommandHandler
{
    public function __construct(
        private readonly UserRepository $repository,
        private readonly EventBus $eventBus
    ) {
    }

    public function handle(Command $command): CommandResponse
    {
        if (!$command instanceof UpdateUserCommand) {
            throw new \InvalidArgumentException();
        }

        $user = $this->repository->update($command->id, $command->email);

        $this->eventBus->dispatch(new UserUpdated($user));

        return CommandResponse::withValue($user->getId());
    }

    public function listenTo(): string
    {
        return UpdateUserCommand::class;
    }

}
