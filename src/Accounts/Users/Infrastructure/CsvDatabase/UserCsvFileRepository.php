<?php

namespace App\Accounts\Users\Infrastructure\CsvDatabase;

use App\Accounts\Users\Domain\User;
use App\Accounts\Users\Domain\UserId;
use App\Accounts\Users\Domain\UserRepository;
use Symfony\Component\HttpKernel\KernelInterface;

class UserCsvFileRepository implements UserRepository
{
    const FILE_PATH = '/var/db-user.test.csv';
    private KernelInterface $kernel;

    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    public function save(User $user): void
    {
        $fp = fopen($this->kernel->getProjectDir() . self::FILE_PATH, 'a');
        fputcsv($fp, [$user->getId()->getValue(), $user->getEmail()]);
        fclose($fp);
    }

    public function update(UserId $id, string $email): User
    {
        $users = $this->findAll();
        foreach ($users as $user) {
            /** @var User $user */
            if ($user->getId() == $id) {
                $user->setEmail($email);
                $this->saveIt($users);
                return $user;
            }
        }
        throw new \RuntimeException('User not found');
    }

    public function find(UserId $id): User
    {
        $users = $this->findAll();
        foreach ($users as $user) {
            /** @var User $user */
            if ($user->getId() == $id) {
                return $user;
            }
        }
        throw new \RuntimeException('User not found');
    }

    public function findAll(): array
    {
        $results = [];
        $fp = fopen($this->kernel->getProjectDir() . self::FILE_PATH, 'r');
        while ($result = fgetcsv($fp)) {
            $results[] = new User(UserId::fromUuidString($result[0]), $result[1]);
        }
        fclose($fp);

        return $results;
    }

    public function delete(UserId $id): void
    {
        $users = $this->findAll();
        $users = array_filter($users, fn(User $user) => $user->getId() != $id);
        $this->saveIt($users);
    }


    protected function saveIt(array $users): void
    {
        $fp = fopen($this->kernel->getProjectDir() . self::FILE_PATH, 'w');
        foreach ($users as $user) {
            fputcsv($fp, [$user->getId()->getValue(), $user->getEmail()]);
        }
        fclose($fp);
    }



}
