<?php

namespace App\Accounts\Users\Domain;

interface UserRepository
{
    public function save(User $user): void;

    /**
     * @return User[]
     */
    public function findAll(): array;

    public function update(UserId $id, string $email): User;

    public function delete(UserId $id): void;

    public function find(UserId $id): User;
}
