<?php

namespace App\Accounts\Users\Domain;

use App\Shared\Domain\ValueObject;
use Ramsey\Uuid\Uuid;

class UserId implements ValueObject
{
    private string $value;

    private function __construct(string $uuidString)
    {
        $this->value = $uuidString;
    }

    public static function new(): self
    {
        return new UserId(Uuid::uuid4()->toString());
    }

    public static function fromUuidString(string $uuid): self
    {
        return new UserId($uuid);
    }

    public function getValue(): string
    {
        return $this->value;
    }
}
