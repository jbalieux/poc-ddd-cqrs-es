<?php

namespace App\Accounts\Users\Domain;

use App\Shared\Domain\DomainEvent;

class UserUpdated implements DomainEvent
{
    public function __construct(public readonly User $user)
    {
    }
}
