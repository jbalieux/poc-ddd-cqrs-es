<?php

namespace App\Accounts\Users\Domain;

use App\Shared\Domain\AggregateRoot;

class User extends AggregateRoot
{
    public function __construct(
        private readonly UserId $id,
        private string $email
    ) {
    }

    public function getId(): UserId
    {
        return $this->id;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }
}
